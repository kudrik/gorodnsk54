<?php

namespace App\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Section;
use App\Entity\District;

class DynamicLoader extends Loader
{
    private $loaded = false;

    private $em;

    private $site_host;

    public function __construct(EntityManagerInterface $em, string $site_host)
    {
        $this->em = $em;
        $this->site_host = $site_host;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routes = new RouteCollection();

        foreach ($this->em->getRepository(District::class)->findAll() as $district) {

            if ($district->getUrl() || $district->getHost()) {

                $routes->add(
                    'district' . $district->getId(),
                    new Route(
                        '/' . $district->getUrl(),
                        [
                            '_controller' => 'App\Controller\DistrictController:index',
                            'districtId' => $district->getId(),
                        ],
                        [],
                        [],
                        $district->getHost() ? $district->getHost() : $this->site_host
                    )
                );
            }
        }

        foreach ($this->em->getRepository(Section::class)->findAll() as $page) {

            if ($page->getUrl() == 'root') {
                continue;
            }

            $urls = array();

            foreach ($page->getAllParents() as $parent) {
                array_unshift($urls, trim($parent->getUrl()));
            }

            $urls[] = trim($page->getUrl());

            $url = mb_strtolower('/' . implode('/', $urls));

            //exclude main page
            if ($url == '/index') {
                $url = '/';
            }

            $route = new Route($url, array(
                '_controller' => 'App\Controller\PageController:index',
                'pageId' => $page->getId(),
            ));

            $routes->add('section' . $page->getId(), $route);
        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'extra' === $type;
    }
}