<?php

namespace App\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters(): array
    {
        return array(
            new \Twig_SimpleFilter('price', [$this, 'priceFilter']),
            new \Twig_SimpleFilter('tel', [$this, 'phoneRusFormater']),
        );
    }

    public function priceFilter(?int $number): ?string
    {
        return number_format($number, 0,'.',' ');
    }

    public function phoneRusFormater(?string $phone): ?string
    {
        if ($phone) {
            if (strlen($phone) === 11) {
                return '+'.substr($phone, 0, 1)
                    . ' (' .substr($phone, 1, 3).') '
                    . substr($phone, 4, 3) . '-'
                    . substr($phone, 7, 2) . '-'
                    . substr($phone, 9, 2);
            }
            return $phone;
        }

        return null;
    }
}