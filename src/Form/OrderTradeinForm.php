<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class OrderTradeinForm extends OrderForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('description', TextareaType::class, array(
            'required' => false,
            'label'=>false,
            'attr'=> array ('placeholder' => 'form.tradein_desc'),
        ));

        parent::buildForm($builder,$options);
    }
}