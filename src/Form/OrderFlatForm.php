<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OrderFlatForm extends OrderForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $subject = $builder->getData();

        $floors = [];
        foreach ($subject->getPlan()->getFlats() as $flat) {
            foreach (explode(',', $flat->getFloors()) as $floor) {
                $floorsRange = explode('-', $floor);
                if (count($floorsRange) > 1) {
                    for ($i = intval($floorsRange[0]); $i <= intval($floorsRange[1]); $i++) {
                        if ($i > 0) {
                            $floors[$i] = $i;
                        }
                    }
                } else {
                    $floors[$floor] = trim($floor);
                }
            }
        }
        $floors = array_unique($floors);
        ksort($floors);

        $builder->add('floor', ChoiceType::class, array(
                'label' => 'form.floor',
                'choices' => $floors,
                'attr' => array('class' => 'e-select_small'),
            )
        );

        $builder->add('description', TextareaType::class, array(
            'required' => false,
            'label' => false,
            'attr' => array('readonly' => 'readonly', 'placeholder' => 'form.flat_desc'),
        ));

        parent::buildForm($builder, $options);
    }
}