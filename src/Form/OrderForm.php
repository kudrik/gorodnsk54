<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Order;
use App\Form\Extension\AntiSpamTypeExtension;

class OrderForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contact_name', TextType::class, [
                'required' => false,
                'attr' => ['placeholder' => 'form.name'],
                'label' => false
            ]
        );

        $builder->add('contact_phone', TextType::class, [
                'required' => true,
                'attr' => AntiSpamTypeExtension::generateAttr($builder->getName(), ['placeholder' => 'form.phone']),
                'label' => false
            ]
        );

        $builder->add('submit', SubmitType::class, [
                'attr' => ['class' => 'b-but'],
                'label' => 'form.send'
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Order::class, 'antispam' => true]);
    }
}