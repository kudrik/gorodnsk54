<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrderExcursionForm extends OrderForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('description', TextType::class, array(
            'required' => false,
            'label'=>false,
            'attr'=> array ('placeholder' => 'form.excursion_date'),
        ));

        parent::buildForm($builder,$options);
    }
}