<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OrderIpotekaForm extends OrderForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('theme', ChoiceType::class, array(
            'choices'=> array(
                'Ипотека'=>'Ипотека',
                'Военная ипотека'=>'Военная ипотека',
                'Ипотека с господдержкой'=>'Ипотека с господдержкой',
                'Материнский капитал'=>'Материнский капитал',
                'Рассрочка от застройщика'=>'Рассрочка от застройщика',
                'Другое'=>'Другое',
            ),
            'label'=>false,
            'placeholder' => 'form.theme',
        ));

        $builder->add('description', TextareaType::class, array(
            'required' => false,
            'label'=>false,
            'attr'=> array ('placeholder' => 'form.question'),
        ));

        parent::buildForm($builder,$options);
    }
}