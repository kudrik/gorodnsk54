<?php

namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AntiSpamTypeExtension extends AbstractTypeExtension
{
    const FIELD_FROM = 'tralala';

    const FIELD_TO = 'pumpurum';

    const SESSION_POSTFIX = '_antispam';

    private $session;

    private $translator;

    public function __construct(SessionInterface $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    static function generateAttr(string $formName, array $attr = []): array
    {
        $attr['onclick'] = 'document.getElementById("' . $formName . '_' . self::FIELD_TO . '").value = document.getElementById("' . $formName . '_' . self::FIELD_FROM . '").value;';
        $attr['onkeypress'] = $attr['onclick'];

        return $attr;
    }

    public function onSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        $secret_from_session = $this->session->get($form->getName() . '_' . self::SESSION_POSTFIX);

        if (!$secret_from_session || $form->get(self::FIELD_TO)->getData() != $secret_from_session) {
            $form->addError(new FormError($this->translator->trans('form.antispam.error')));
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['antispam']) {
            return;
        }

        $builder->add(self::FIELD_FROM, HiddenType::class, ['mapped' => false]);

        $builder->add(self::FIELD_TO, HiddenType::class, ['mapped' => false]);

        $builder->addEventListener(FormEvents::SUBMIT, [$this, 'onSubmit']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (!$options['antispam'] || $form->isSubmitted()) {
            return;
        }

        $secret_key_value = md5('trolololalilala' . time());

        $this->session->set($form->getName() . '_' . self::SESSION_POSTFIX, $secret_key_value);

        $form->get(self::FIELD_FROM)->setData($secret_key_value);
    }

    public function getExtendedType()
    {
        return FormType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['antispam' => false]);
    }
}