<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use App\Entity\Order;
use App\Form\Extension\AntiSpamTypeExtension;

class OrderCallmeAdvForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contact_name', TextType::class, [
                'required' => false,
                'attr' => ['placeholder' => 'form.name'],
                'label' => false
            ]
        )->add('contact_phone', TextType::class, [
                'attr' => AntiSpamTypeExtension::generateAttr($builder->getName(), ['placeholder' => 'form.phone']),
                'label' => false
            ]
        )->add('description', TextareaType::class, [
                'required' => false,
                'label' => false,
                'attr' => ['placeholder' => 'form.question']
            ]
        )->add('submit', SubmitType::class, ['label' => 'form.send', 'attr' => ['class' => 'b-but']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'antispam' => true
        ]);
    }
}