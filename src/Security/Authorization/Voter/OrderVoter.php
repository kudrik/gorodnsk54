<?php

namespace App\Security\Authorization\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use App\Entity\Order;

class OrderVoter extends Voter
{
    protected function supports($attribute, $subject): bool
    {
        return $subject instanceof Order;
    }

    protected function voteOnAttribute($attribute, $order, TokenInterface $token): bool
    {
        if (!$order->getDistrict()) {
            return true;
        }

        $user = $token->getUser();

        return $order->getDistrict()->getUsers()->exists(
            function($key, User $element) use ($user){
                return $user->getId() === $element->getId();
            }
        );
    }
}