<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserRegistrationForm;
use App\Form\UserRestoreForm;
use App\Entity\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function login(AuthenticationUtils $authenticationUtils)
    {
        return $this->render('security/login.html.twig', [
            // last username entered by the user (if any)
            'last_email' => $authenticationUtils->getLastUsername(),
            // last authentication error (if any)
            'error' => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    public function logout()
    {
        throw new \Exception('This should never be reached!');
    }

    public function registration(Request $request)
    {
        // 1) build the form

        $form = $this->createForm(UserRegistrationForm::class);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            // 3) Encode the password -  it is happening  via Doctrine listener)

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('login');
        }



        return $this->render('security/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function restore(Request $request)
    {
        $form = $this->createForm(UserRestoreForm::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $manager = $this->getDoctrine()->getManager();

            if ($user = $this->getDoctrine()->getRepository(User::class)->loadUserByUsername($data['email'])) {

                $user->setRestore(md5(time().'dss'.$user->getId()));

                $datetime = new \DateTime();
                $datetime->modify('+1 day');

                $user->setRestoreTo($datetime);
            }

            $manager->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Restore password')
                ->setFrom('send@example.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'security/email.restore.html.twig',
                        array('User' => $user)
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);

            return $this->render('security/restore.done.html.twig');
        }

        return $this->render('security/restore.html.twig', [
            'form' => $form->createView()
        ]);

    }


    public function restoreHandle(Request $request)
    {
        $email = $request->get('email');
        $hash = $request->get('hash');

        $manager = $this->getDoctrine()->getManager();

        if ($user = $this->getDoctrine()->getRepository(User::class)->loadUserByUsername($email)) {

            if ($user->getRestore() && $user->getRestore()==$hash) {

                $user->setRestore('');

                //$manager->flush();

                echo 'dsd';

            }
        }
    }
}
