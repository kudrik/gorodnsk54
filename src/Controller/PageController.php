<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Section;
use App\Service\District as DistrictService;

class PageController extends AbstractController
{
    public function index(int $pageId, DistrictService $districtService)
    {
        $SectionRepositary = $this->getDoctrine()->getRepository(Section::class);

        if ($Section = $SectionRepositary->find($pageId)) {

            return $this->render('default/page.html.twig', [
                'Page' => $Section,
                'districts' => $districtService->getAll(),
            ]);
        }
    }
}