<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\District;
use App\Entity\Building;
use App\Entity\Plan;
use App\Entity\Order;
use App\Entity\Flat;
use App\Form\OrderIpotekaForm;
use App\Service\District as DistrictService;
use Symfony\Component\HttpFoundation\Response;

class DistrictController extends AbstractController
{
    public function index(int $districtId, DistrictService $districtService)
    {
        if ($district = $this->getDoctrine()->getRepository(District::class)->findOneBy(
            array(
                'id' => $districtId,
                'is_public' => 1,
            )
        )) {

            $viewVars = ['Page' => $district, 'plans' => [], 'buildings' => [], 'districts' => $district->showRelated() ? $districtService->getAll() : []];

            $viewVars['ipotekaForm'] = $this->createForm(OrderIpotekaForm::class, new Order(), [
                'action' => $this->generateUrl('ipoteka_form', ['district_id' => $district->getId()])
            ])->createView();

            $viewVars['advancedMenuItems'] = [
                ['name' => 'Планировки и цены', 'uri' => '#flats'],
                ['name' => 'Ипотека', 'uri' => '#ipoteka'],
                ['name' => 'Квартира в зачет', 'route' => 'tradein_form', 'routeParameters' => ['district_id' => $district->getId()]]
            ];

            if ($district->getBuildings()->count() > 1) {
                $viewVars['buildings'] = $this->getDoctrine()->getRepository(Building::class)->findByDistrictWithFlats($district);
            } else {
                $viewVars['plans'] = $this->getDoctrine()->getRepository(Plan::class)->findByDistrictWithMinPrice($district);
            }

            return $this->render('districts/index.html.twig', $viewVars);

        } else {
            throw $this->createNotFoundException('Страница не найдена');
        }
    }
}