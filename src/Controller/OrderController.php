<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormInterface;
use App\Form\OrderForm;
use App\Form\OrderExcursionForm;
use App\Form\OrderTradeinForm;
use App\Form\OrderIpotekaForm;
use App\Form\OrderFlatForm;
use App\Form\OrderCallmeAdvForm;
use App\Entity\Order;
use App\Entity\Plan;
use App\Entity\Section;
use App\Entity\District;

class OrderController extends AbstractController
{
    /**
     * @var District
     */
    private $district;

    private function setDistrictById(?int $district_id)
    {
        if ($district_id) {
            $this->district = $this->getDoctrine()->getRepository(District::class)->find($district_id);
        }
    }

    private function _formSubmit(FormInterface $form, Request $request): ?Response
    {
        $form->handleRequest($request);
        $order = $form->getData();
        $order->setUserinfo(@$_SERVER['REMOTE_ADDR']);
        $order->setReferrer(@$_SERVER['HTTP_REFERER']);
        if ($this->district) {
            $order->setDistrict($this->district);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            return $this->render('order/done.html.twig');
        }

        return null;
    }

    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $parameters['styles'] = $this->district ? $this->district->getStyles() : '';
        $parameters['district_id'] = $this->district ? $this->district->getId() : null;
        $parameters['linkToIndex'] = $this->district ? $this->generateUrl('district' . $this->district->getId()) : null;

        return parent::render($view, $parameters, $response);
    }

    public function callme(?int $district_id, Request $request)
    {
        $this->setDistrictById($district_id);

        $order = (new Order())->setTheme('Обратный звонок');

        $form = $this->createForm(OrderForm::class, $order, ['action' => $this->generateUrl($request->get('_route'), ['district_id' => $district_id])]);

        if ($response = $this->_formSubmit($form, $request)) {
            return $response;
        }

        return $this->render('order/callme.html.twig', ['form' => $form->createView()]);
    }

    public function excursion(?int $district_id, Request $request)
    {
        $this->setDistrictById($district_id);

        $order = (new Order())->setTheme('Экскурсия');

        $form = $this->createForm(OrderExcursionForm::class, $order, ['action' => $this->generateUrl($request->get('_route'), ['district_id' => $district_id])]);

        if ($response = $this->_formSubmit($form, $request)) {
            return $response;
        }

        return $this->render('order/excursion.html.twig', ['form' => $form->createView()]);
    }

    public function tradein(?int $district_id, Request $request)
    {
        $order = new Order();

        if ($Section = $this->getDoctrine()->getRepository(Section::class)->findOneByUrl('tradein')) {
            $order->setTheme($Section->getName());
        }

        $this->setDistrictById($district_id);

        $form = $this->createForm(OrderTradeinForm::class, $order,
            ['action' => $this->generateUrl($request->get('_route'), ['district_id' => $district_id])]
        );

        if ($response = $this->_formSubmit($form, $request)) {
            return $response;
        }

        return $this->render('order/tradein.html.twig', [
            'Page' => $Section,
            'form' => $form->createView()
        ]);
    }

    public function ipoteka(int $district_id, Request $request)
    {
        $this->setDistrictById($district_id);

        $form = $this->createForm(OrderIpotekaForm::class, new Order(),
            ['action' => $this->generateUrl($request->get('_route'), ['district_id' => $district_id])]
        );

        if ($response = $this->_formSubmit($form, $request)) {
            return $response;
        }

        return $this->render('order/ipoteka.html.twig', ['form' => $form->createView()]);
    }

    public function callmeAdv(?int $district_id, Request $request)
    {
        $this->setDistrictById($district_id);

        $order = (new Order())->setTheme('Заявка');

        $form = $this->createForm(OrderCallmeAdvForm::class, $order, ['action' => $this->generateUrl($request->get('_route'), ['district_id' => $district_id])]);

        if ($response = $this->_formSubmit($form, $request)) {
            return $response;
        }

        return $this->render('order/callme.adv.html.twig', ['form' => $form->createView()]);
    }

    public function plan(int $plan_id, Request $request)
    {
        $order = (new Order())->setTheme('Бронирование квартиры');

        if ($plan = $this->getDoctrine()->getRepository(Plan::class)->find($plan_id)) {

            $order->setPlan($plan);

            $description = $plan->getFullName().PHP_EOL;

            foreach ($plan->getFlats() as $flat) {
                $description.= PHP_EOL . $flat->getDescription();
            }

            $order->setDescription($description);

            $this->district = $plan->getBuilding()->getDistrict();

            $form = $this->createForm(OrderFlatForm::class, $order,
                ['action' => $this->generateUrl($request->get('_route'), ['plan_id' => $plan_id, 'district_id' => $this->district->getId()])]
            );

            if ($response = $this->_formSubmit($form, $request)) {
                return $response;
            }

            return $this->render('order/flat.html.twig', ['form' => $form->createView()]);
        }
    }
}