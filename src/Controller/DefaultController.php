<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Section;
use App\Service\District as DistrictService;

class DefaultController extends AbstractController
{
    public function index(DistrictService $districtService)
    {
        $SectionRepositary = $this->getDoctrine()->getRepository(Section::class);

        if ($Section = $SectionRepositary->findOneByUrl('index')) {

            return $this->render('default/index.html.twig', [
                'Page' => $Section,
                'districts' => $districtService->getAll(),
            ]);
        }
    }
}