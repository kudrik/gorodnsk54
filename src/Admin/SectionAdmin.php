<?php
namespace App\Admin;

use App\Entity\Section;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class SectionAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'lft',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Content', array('class' => 'col-md-6'))
                ->add('parent', EntityType::class, array(
                    'class' => Section::class,
                    'choice_label' => 'nameWithLevel',
                ))
                ->add('name', TextType::class)
                ->add('url', TextType::class, array('required' => false,))
            ->end()
            ->with('Seo', array('class' => 'col-md-6'))
                ->add('meta_title', TextType::class, array('required' => false,))
                ->add('meta_key', TextType::class, array('required' => false,))
                ->add('meta_desc', TextType::class, array('required' => false,))
            ->end()
            ->with('Description', array('class' => 'col-md-12'))
                ->add('description', TextareaType::class, array('required' => false,'attr'=>array('class'=>'mceAdvanced')))
            ->end()
            ->with('Advanced', array('class' => 'col-md-4'))
                ->add('show_tit', CheckboxType::class, array('required' => false,))
                ->add('show_sub', CheckboxType::class, array('required' => false,))
                //->add('type',     'choice', array('required' => false, 'choices'=>array('Блок на главной'=>1)))
                ->add('position', IntegerType::class, array('required' => false))
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('url');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('nameWithLevel');
        $listMapper->addIdentifier('url');
    }
}