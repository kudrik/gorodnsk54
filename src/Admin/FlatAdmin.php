<?php

namespace App\Admin;

use App\Entity\Building;
use App\Entity\District;
use App\Entity\Flat;
use App\Entity\Plan;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FlatAdmin extends DistrictChildCommonAdmin
{
    protected function buildForm()
    {
        /** @var Flat $flat */
        $flat = $this->getSubject();

        if (!$flat->getId()) {

            $district = $this->getParent()->getSubject();

            if ($district instanceof District) {
                /** @var Building $defaultBuilding */
                $defaultBuilding = $district->getBuildings()->first();

                if (!$defaultBuilding) {
                    throw new \Exception('There are no buildings for this district');
                }

                $defaultPlan = $defaultBuilding->getPlans()->first();

                if (!$defaultPlan) {
                    throw new \Exception('There are no plans for this district');
                }

                $flat->setPlan($defaultPlan);
            }
        }

        return parent::buildForm();
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var Flat $flat */
        $flat = $this->getSubject();

        $formMapper->add('plan', EntityType::class, [
            'class' => Plan::class,
            'choices' => $flat->getPlan()->getBuilding()->getDistrict()->getPlans(),
            'choice_label' => 'getFullName',
            'group_by' => 'building.name',
        ]);
        $formMapper->add('floors', TextType::class, ['help' => 'Введите этажи через запятую']);
        $formMapper->add('price', IntegerType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('plan.building', null, array(), EntityType::class, [
            'class' => Building::class,
            'choice_label' => 'name',
        ]);
        $datagridMapper->add('floors');
        $datagridMapper->add('price');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('plan.building.district.name');
        $listMapper->addIdentifier('plan.building.name');
        $listMapper->addIdentifier('plan.imgPreviewTag', 'html');
        $listMapper->addIdentifier('plan.name');
        $listMapper->addIdentifier('plan.s_total');
        $listMapper->addIdentifier('plan.s_living');
        $listMapper->addIdentifier('plan.s_kitchen');
        $listMapper->add('floors', null, array('editable' => true));
        $listMapper->add('price', null, array('editable' => true));
    }

    public function getExportFields()
    {
        return ['plan.building.district.name', 'plan.building.name', 'plan.building.id', 'plan.rooms', 'plan.squares', 'floors', 'price'];
    }

    public function getExportFormats()
    {
        return ['csv', 'xml'];
    }

    public function getDataSourceIterator()
    {
        $datagrid = $this->getDatagrid();

        $datagrid->buildPager();

        $proxyQuery = $datagrid->getQuery();
        $proxyQuery->innerJoin('o.plan', 'p');
        $proxyQuery->innerJoin('p.building', 'b');
        $proxyQuery->innerJoin('b.district', 'd');
        $proxyQuery->addOrderBy('d.position', 'asc');
        $proxyQuery->addOrderBy('b.position', 'asc');
        $proxyQuery->addOrderBy('p.rooms', 'asc');
        $proxyQuery->addOrderBy('o.price', 'asc');

        return $this->getModelManager()->getDataSourceIterator($datagrid, $this->getExportFields());
    }
}