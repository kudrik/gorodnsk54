<?php

namespace App\Admin\Sortable;

use Doctrine\ORM\QueryBuilder;

interface SortableAdminInterface
{
    public function createSortableAdminQuery(): QueryBuilder;
}