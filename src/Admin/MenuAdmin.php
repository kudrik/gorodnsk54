<?php

namespace App\Admin;

use App\Entity\Section;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class MenuAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('section', EntityType::class, array(
            'class' => Section::class,
            'choice_label' => 'name',
            'required' => false,
        ));
        $formMapper->add('name', TextType::class, array('required' => false,));
        $formMapper->add('url', TextType::class, array('required' => false,));
        $formMapper->add('position', IntegerType::class, array('required' => false,));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('section', null, array(), EntityType::class, array(
            'class' => Section::class,
            'choice_label' => 'name',
        ));
        $datagridMapper->add('name');
        $datagridMapper->add('url');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('section.name');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('url');

    }
}