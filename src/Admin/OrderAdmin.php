<?php
namespace App\Admin;

use App\Entity\District;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class OrderAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('theme', TextType::class);
        $formMapper->add('contact_name', TextType::class);
        $formMapper->add('contact_phone', TextType::class);
        $formMapper->add('description', TextareaType::class);
        $formMapper->add('userinfo', TextType::class, [ 'disabled'  => true,]);
        $formMapper->add('referrer', TextType::class, [ 'disabled'  => true,]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('district', null, [], EntityType::class, [
            'class'    => District::class,
            'choice_label' => 'name',
        ]);
        $datagridMapper->add('theme');
        $datagridMapper->add('contact_name');
        $datagridMapper->add('contact_phone');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('created');
        $listMapper->addIdentifier('district.name');
        $listMapper->addIdentifier('theme');
        $listMapper->addIdentifier('contact_name');
        $listMapper->addIdentifier('contact_phone');
    }
    
    public function configureShowFields(ShowMapper $showMapper)
    {
    	$showMapper
    	->add('theme')
        ->add('district.name')
    	->add('contact_name')
    	->add('contact_phone')
        ->add('description')
        ->add('userinfo')
        ->add('referrer');
    }

    public function createQuery($context = 'list')
    {
        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = parent::createQuery($context);

        $container = $this->getConfigurationPool()->getContainer();

        if (!$container->get('security.authorization_checker')->isGranted(['ROLE_SUPER_ADMIN'])) {

            $districtIds = [];
            foreach ($container->get('security.token_storage')->getToken()->getUser()->getDistricts() as $district) {
                $districtIds[] = $district->getId();
            }

            $query->andWhere(
                $query->expr()->in($query->getRootAliases()[0] . '.district', $districtIds)
            );
            $query->orWhere(
                $query->expr()->isNull($query->getRootAliases()[0] . '.district')
            );
        }

        return $query;
    }
}