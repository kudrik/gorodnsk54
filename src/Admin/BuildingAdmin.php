<?php
namespace App\Admin;

use App\Admin\Sortable\SortableAdminInterface;
use App\Entity\Building;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class BuildingAdmin extends DistrictChildCommonAdmin implements SortableAdminInterface
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    public function createSortableAdminQuery(): QueryBuilder
    {
        return $this->createQuery()->andWhere('o.district = :district')->setParameter('district', $this->getSubject()->getDistrict());
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    protected function setDefaultPosition(Building $building)
    {
        if (!($building->getId() > 0)) {
            $last = $building->getDistrict()->getBuildings()->last();

            $building->setPosition(
                ($last ? $last->getPosition() : 0) + 10
            );
        }
    }


    protected function configureFormFields(FormMapper $formMapper)
    {
        $building = $this->getSubject();

        $this->setDefaultPosition($building);

        $formMapper->add('name', TextType::class);
        $formMapper->add('address', TextType::class, array('required' => false,));
        $formMapper->add('announce', TextType::class, array('required' => false,));
        $formMapper->add('compl_time', TextType::class, array('required' => false,));
        $formMapper->add('material', TextType::class, array('required' => false,));
        $formMapper->add('floors', TextType::class, array('required' => false,));
        $formMapper->add('description', TextType::class, array('required' => false,));
        //$formMapper->add('image', FileType::class, array('required' => false));
        $formMapper->add('position', IntegerType::class, array('required' => false,));
        $formMapper->add('image', FileType::class, array('required' => false, 'help'=>$building->getImgPreviewTag()));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('district.name');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('address');
        $listMapper->add('Plans', 'actions', array(
            'actions' => array(
                'plans' => array('template' => 'admin/building_plans.html.twig')
            )
        ));
        $listMapper->add('_action', null, [
            'actions' => [
                'move' => [
                    'template' => 'admin/_sort.html.twig',
                ],
            ]
        ]);
    }
}