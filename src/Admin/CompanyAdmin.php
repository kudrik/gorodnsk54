<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CompanyAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        $formMapper->add('name', TextType::class);
        $formMapper->add('url', TextType::class, array('required' => false,));
        $formMapper->add('description', TextareaType::class, array('required' => false, 'attr' => array('class' => 'mceAdvanced')));
        $formMapper->add('image', FileType::class, array('required' => false, 'help'=>$subject->getImgPreviewTag()));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('imgPreviewTag','html');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('url');
        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'comments' => array('template' => 'admin/company_districts.html.twig')
            )
        ));
    }
}