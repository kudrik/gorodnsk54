<?php

namespace App\Admin;

use App\Entity\Building;
use App\Entity\District;
use App\Entity\Plan;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Object\Metadata;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface as RoutingUrlGeneratorInterface;

class PlanAdmin extends DistrictChildCommonAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'rooms',
    );

    protected function buildForm()
    {
        /** @var Plan $plan */
        $plan = $this->getSubject();

        if (!$plan->getId()) {

            $district = $this->getParent()->getSubject();

            if ($district instanceof District) {
                $defaultBuilding = $district->getBuildings()->first();

                if (!$defaultBuilding) {
                    throw new \Exception('There are no buildings for this district');
                }

                $plan->setBuilding($defaultBuilding);
            }
        }

        return parent::buildForm();
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var Plan $plan */
        $plan = $this->getSubject();

        $formMapper->add('building', EntityType::class, array(
            'class' => Building::class,
            'choices' => $plan->getBuilding()->getDistrict()->getBuildings(),
            'choice_label' => 'name',
            'group_by' => 'district.name',
        ));

        $formMapper->add('rooms', IntegerType::class);
        $formMapper->add('s_total', NumberType::class, array('scale' => 2,));
        $formMapper->add('s_living', NumberType::class, array('scale' => 2,));
        $formMapper->add('s_kitchen', NumberType::class, array('scale' => 2,));
        $formMapper->add('loggia', CheckboxType::class, array('required' => false));
        $formMapper->add('s_loggia', NumberType::class, array('scale' => 2, 'required' => false));
        $formMapper->add('description', TextType::class, array('required' => false,));
        $formMapper->add('image', FileType::class, array('required' => false, 'help' => $plan->getImgPreviewTag()));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('building', null, [], EntityType::class, [
            'class' => Building::class,
            'choice_label' => 'name',
        ]);
        $datagridMapper->add('rooms');
        $datagridMapper->add('s_total');
        $datagridMapper->add('s_living');
        $datagridMapper->add('s_kitchen');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('building.district.name');
        $listMapper->addIdentifier('building.name');
        $listMapper->addIdentifier('imgPreviewTag', 'html');
        $listMapper->addIdentifier('rooms');
        $listMapper->addIdentifier('s_total');
        $listMapper->addIdentifier('s_living');
        $listMapper->addIdentifier('s_kitchen');
        $listMapper->addIdentifier('s_loggia');
        $listMapper->add('_action', 'actions', [
            'actions' => [
                'flats' => ['template' => 'admin/plan_flats.html.twig']
            ]
        ]);
    }
}