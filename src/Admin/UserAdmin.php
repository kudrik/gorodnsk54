<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Sonata\AdminBundle\Form\Type\ModelType;
use App\Entity\User;

class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('email', EmailType::class);

        $subject = $this->getSubject();

        $formMapper->add('plainPassword', TextType::class, ['required' => !boolval($subject->getId())]);

        $formMapper->add('roles', ChoiceType::class,  [
            'choices'  => array_combine(User::ROLES, User::ROLES),
            'expanded' => false,
            'multiple' => true,
        ]);

        $formMapper->add('districts', ModelType::class, ['required' => false, 'multiple' => true,   'property' => 'name', 'btn_add' => false]);

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('email');
        $listMapper->addIdentifier('getRolesString', null, ['label'=>'Role']);
        $listMapper->add('districts', null, ['associated_property' => 'getName', 'identifier' => true]);

    }
}