<?php

namespace App\Admin;

use App\Admin\Sortable\SortableAdminInterface;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use App\Entity\Photo;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class PhotoAdmin extends DistrictChildCommonAdmin implements SortableAdminInterface
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    public function createSortableAdminQuery(): QueryBuilder
    {
        return $this->createQuery()->andWhere('o.district = :district')->setParameter('district', $this->getSubject()->getDistrict());
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    protected function setDefaultPosition(Photo $photo)
    {
        if (!($photo->getId() > 0)) {
            $last = $photo->getDistrict()->getPhotos()->last();

            $photo->setPosition(
                ($last ? $last->getPosition() : 0) + 10
            );
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class, array('required' => false,));

        $photo = $this->getSubject();
        $this->setDefaultPosition($photo);

        if ($photo->getId() > 0) {
            $formMapper->add('image', FileType::class, array('required' => false, 'help' => $photo->getImgPreviewTag()));
        } else {
            $formMapper->add('image', FileType::class);
        }
        $formMapper->add('position', IntegerType::class, array('required' => false,));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('imgPreviewTag', 'html');
        $listMapper->addIdentifier('district.name');
        $listMapper->addIdentifier('name');
        $listMapper->add('_action', null, [
            'actions' => [
                'move' => [
                    'template' => 'admin/_sort.html.twig',
                ],
            ]
        ]);
    }
}