<?php

namespace App\Admin;

use App\Entity\Company;
use App\Entity\District;
use App\Entity\Photo;
use App\Entity\User;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DistrictAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function createSortableAdminQuery()
    {
        return $this->createQuery();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    protected function setDefaultPosition(District $district)
    {
        if (!($district->getId() > 0)) {
            $district->setPosition(time());
        }
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $admin->getRequest()->get('id');

        $district = $this->getObject($id);

        if ($district) {

            /** @var MenuItemInterface $menu */
            $menu = $this->getConfigurationPool()->getContainer()->get('sonata.admin.sidebar_menu');

            $districtMenu = $menu->getChild('Menu')->getChild('Districts');

            $districtMenu = $districtMenu->addChild($district->getName(), []);

            $districtMenu->addChild('Photos', [
                'route' => 'admin_app_district_photo_list',
                'routeParameters' => ['id' => $id],
            ]);

            $districtMenu->addChild('Buildings', [
                'route' => 'admin_app_district_building_list',
                'routeParameters' => ['id' => $id]
            ]);

            $districtMenu->addChild('Plans', [
                'route' => 'admin_app_district_plan_list',
                'routeParameters' => ['id' => $id]
            ]);

            $districtMenu->addChild('Flats', [
                'route' => 'admin_app_district_flat_list',
                'routeParameters' => ['id' => $id]
            ]);
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var District $district */
        $district = $this->getSubject();

        $this->setDefaultPosition($district);

        $formMapper->add('company', EntityType::class, array(
            'class' => Company::class,
            'choice_label' => 'name',
        ));
        $formMapper->add('name', TextType::class);
        $formMapper->add('host', TextType::class, array('required' => false,));
        $formMapper->add('url', TextType::class, array('required' => false,));
        $formMapper->add('announce', TextType::class, array('required' => false,));
        $formMapper->add('mapCoords', TextType::class, array('required' => false,));
        $formMapper->add('phone', TelType::class, ['required' => false, 'attr' => ['placeholder' => '+7-XXX-XXX-XX-XX']]);
        $formMapper->add('mobile', TelType::class, ['required' => false, 'attr' => ['placeholder' => '+7-XXX-XXX-XX-XX']]);
        $formMapper->add('description', TextareaType::class, array('required' => false, 'attr' => array('class' => 'mceAdvanced')));
        $formMapper->add('ipoteka', TextareaType::class, array('required' => false, 'attr' => array('class' => 'mceAdvanced')));
        $formMapper->add('tags', ModelType::class, array('required' => false, 'multiple' => true, 'property' => 'name'));
        $formMapper->add('isPublic', CheckboxType::class, array('required' => false));
        $formMapper->add('showRelated', CheckboxType::class, array('required' => false, 'label' => 'Show other districts'));
        $formMapper->add('position', IntegerType::class, array('required' => false,));
        $formMapper->add('image', FileType::class, array('required' => false, 'help' => $district->getImgPreviewTag()));
        $formMapper->add('metrika', TextareaType::class, ['required' => false]);

        $formMapper->add('styles', TextareaType::class, ['required' => false, 'attr' => ['class' => 'cssTextArea']]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('company', null, array(), EntityType::class, array(
            'class' => Company::class,
            'choice_label' => 'name',
        ));
        $datagridMapper->add('name');
        $datagridMapper->add('users', null, array(), EntityType::class, array(
            'class' => User::class,
            'choice_label' => 'email',
        ));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('company.name');
        $listMapper->add('users', null, ['associated_tostring' => 'getEmail']);


        $listMapper->add('isPublic', 'boolean', array('editable' => true));
        $listMapper->add('Photo', 'actions', [
            'actions' => [
                'photos' => ['template' => 'admin/district_photos.html.twig'],
            ]
        ]);
        $listMapper->add('Buildings', 'actions', [
            'actions' => [
                'buildings' => ['template' => 'admin/district_buildings.html.twig'],
            ]
        ]);
        $listMapper->add('Flats', 'actions', [
            'actions' => [
                'buildings' => ['template' => 'admin/district_flats.html.twig'],
            ]
        ]);
        $listMapper->add('_action', 'actions', [
            'actions' => [
                'move' => [
                    'template' => 'admin/_sort.html.twig',
                ],
            ]
        ]);
    }
}