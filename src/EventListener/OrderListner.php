<?php

namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Entity\Order;

class OrderListner
{
    protected $twig;
    protected $mailer;
    protected $mailfrom;
    protected $mailToList;
    protected $siteName;

    public function __construct(\Twig_Environment $twig, \Swift_Mailer $mailer, String $mailFrom, array $mailTo, String $siteName)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->mailFrom = $mailFrom;
        $this->mailToList = $mailTo;
        $this->siteName = $siteName;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $order = $args->getEntity();

        if ($order instanceof Order) {

            if ($district = $order->getDistrict()) {
                $order->setTheme($order->getTheme() . ' - ' . $district->getName());
            }

            if ($order->getFloor()) {
                $order->setDescription($order->getDescription() . PHP_EOL . PHP_EOL . 'Выбранный этаж: ' . $order->getFloor());
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $order = $args->getEntity();

        if ($order instanceof Order) {

            $mailTo = [];

            if ($order->getDistrict()) {
                foreach ($order->getDistrict()->getUsers() as $user) {
                    $mailTo[] = $user->getEmail();
                }
            }

            if (!(count($mailTo) > 0)) {
                $mailTo = $this->mailToList;
            }

            $this->mailer->send(
                (new \Swift_Message())
                    ->setSubject('Заказ с сайта ' . $this->siteName)
                    ->setFrom($this->mailFrom)
                    ->setTo($mailTo)
                    ->setBody(
                        $this->twig->render(
                            'order/email.html.twig',
                            array('Order' => $order)
                        ),
                        'text/html')
            );
        }
    }
}