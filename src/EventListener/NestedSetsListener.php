<?php

namespace App\EventListener;

use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Traits\NestedSetsTrait;

class NestedSetsListener
{
    protected $doctrine;

    protected $singleton = true;

    function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function postPersist($args)
    {
        $this->build($args);
    }

    public function postUpdate($args)
    {
        $this->build($args);
    }

    public function postRemove($args)
    {
        $this->build($args);
    }

    protected function isValid($entity)
    {
        return in_array(NestedSetsTrait::class, class_uses($entity));
    }


    protected function tree($items, int $lft, int $level)
    {
        $level++;

        if ($items) {

            foreach ($items as $item) {

                //echo $item->getName() . ' - ' . $item->getPosition() .' - ' . $item->getLft(). ':'.$item->getRgt() .'<br>';
                $lft++;

                $item->setLft($lft);

                $item->setLvl($level);

                $lft = $this->tree($item->getChildren(), $lft, $level);

                $item->setRgt($lft);
            }
        }

        return $lft + 1;
    }

    protected function build($args)
    {
        if ($this->isValid($args->getEntity()) && $this->singleton) {

            $this->singleton = false;

            $root = $this->doctrine->getRepository(get_class($args->getEntity()))->findOneByParent(null);

            $this->tree($root->getChildren(), intval($root->getLft()), intval($root->getLvl()));

            $args->getEntityManager()->flush();
        }
    }
}