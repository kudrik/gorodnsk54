<?php
namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserListner
{
    private $password_encoder;

    public function __construct(UserPasswordEncoderInterface $password_encoder, \Swift_Mailer $mailer)
    {
        $this->password_encoder = $password_encoder;
        $this->mailer = $mailer;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->hashPassword($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->hashPassword($args);
    }

    protected function hashPassword(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // only act on some "User" entity
        if ($entity instanceof User) {

            if ($entity->getPlainPassword()) {

                $entity->setPassword(
                    $this->password_encoder->encodePassword($entity, $entity->getPlainPassword())
                );
            }
        }
    }
}