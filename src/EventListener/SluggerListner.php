<?php
namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Utils\Slugger;
use App\Entity\Section;
use App\Entity\Company;
use App\Entity\District;

class SluggerListner
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->process($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->process($args);
    }

    private function process(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // only act on some "Section" entity
        if ($entity instanceof Section || $entity instanceof Company) {

            if (!($url = $entity->getUrl())) {

                $url = $entity->getName();
            }

            $entity->setUrl(Slugger::slugify($url));

            $entity->getUrl();

            return;
        } elseif ($entity instanceof District) {
            if (!($url = $entity->getUrl()) && !$entity->getHost()) {

                $url = $entity->getName();
            }

            $entity->setUrl(Slugger::slugify($url));

            $entity->getUrl();

            return;
        }
    }
}