<?php

namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

class PositionListner
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->process($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->process($args);
    }

    protected function process(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (method_exists($entity, 'getPosition') && method_exists($entity, 'setPosition') && !($entity->getId() > 0) && !($entity->getPosition() > 0)) {

            $highest_position = $args->getEntityManager()->createQueryBuilder()->select('MAX(e.position)')->from(get_class($entity), 'e')
                ->getQuery()
                ->getSingleScalarResult();

            $entity->setPosition($highest_position + 10);
        }
    }
}