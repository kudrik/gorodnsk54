<?php

namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Entity\Section;
use App\Entity\Menu;
use App\Entity\District;

class ClearCacheService
{
    private $project_dir;

    private $environment;

    public function __construct(string $project_dir, string $environment)
    {
        $this->project_dir = $project_dir;
        $this->environment = $environment;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->clear($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->clear($args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->clear($args);
    }

    protected function clear(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // only act on entity which affect router
        if ($entity instanceof Section || $entity instanceof Menu || $entity instanceof District) {
            exec($this->project_dir . '/bin/console cache:clear --no-warmup --env=' . $this->environment);
        }
    }
}