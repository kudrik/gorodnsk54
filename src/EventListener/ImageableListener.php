<?php

namespace App\EventListener;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Filesystem\Filesystem;
use Intervention\Image\ImageManager;
use App\Entity\Traits\ImageableTrait;

class ImageableListener
{
    private $web_dir;

    public function __construct(string $web_dir)
    {
        $this->web_dir = $web_dir;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postUpdate($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->uploadImage($entity);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->removeImage($entity);
    }


    protected function isImageable($entity)
    {
        return in_array(ImageableTrait::class, class_uses($entity));
    }

    protected function uploadImage($entity)
    {
        if ($this->isImageable($entity) && $entity->getImage()) {

            $imageManager = new ImageManager();

            foreach ($entity->getImageSizes() as $k=>$size) {

                if ($size) {

                    $method_name = 'getImage'.$k.'Name';

                    if ($size[0] || $size[1]) {

                        $img = $imageManager->make($entity->getImage());

                        $img->resize($size[0], $size[1], function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        $img->save($this->web_dir . $entity->$method_name(), $size[2]);

                    } elseif ($entity->getImageExt() == 'svg' || getimagesize($entity->getImage())) {

                        copy($entity->getImage(), $this->web_dir . $entity->$method_name());

                    }
                }
            }
        }
    }

    protected function removeImage($entity)
    {
        if ($this->isImageable($entity)) {

            $fs = new Filesystem();
            $fs->remove(array(
                $this->web_dir . $entity->getImageOrigName(),
                $this->web_dir . $entity->getImageLName(),
                $this->web_dir . $entity->getImageMName(),
                $this->web_dir . $entity->getImageSName(),
            ));
        }
    }
}