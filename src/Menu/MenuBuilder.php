<?php

namespace App\Menu;

use App\Entity\Menu;
use Knp\Menu\FactoryInterface;
use Knp\Menu\MenuItem;
use Symfony\Bridge\Doctrine\RegistryInterface;

class MenuBuilder
{
    private $factory;

    private $doctrine;

    public function __construct(FactoryInterface $factory, RegistryInterface $doctrine)
    {
        $this->factory = $factory;
        $this->doctrine = $doctrine;
    }

    public function createMainMenu(array $options): MenuItem
    {
        $menu = $this->factory->createItem('root');

        $menuRepository = $this->doctrine->getRepository(Menu::class);

        //advansed items menu (for example in districts)
        if (isset($options['advancedItems']) && $options['advancedItems']) {
            foreach ($options['advancedItems'] as $item) {
                if (isset($item['uri'])) {
                    $menu->addChild($item['name'], ['uri' => $item['uri']]);
                } else {
                    $menu->addChild($item['name'], ['route' => $item['route'], 'routeParameters' => isset($item['routeParameters']) ? $item['routeParameters'] : []]);
                }
            }
        }

        foreach ($menuRepository->findBy([], ['position' => 'ASC']) as $item) {
            if ($item->getUrl()) {
                $menu->addChild($item->getFullName(), ['uri' => $item->getUrl()]);
            } else {
                $menu->addChild($item->getFullName(), ['route' => 'section' . $item->getSection()->getId()]);
            }
        }

        return $menu;
    }
}