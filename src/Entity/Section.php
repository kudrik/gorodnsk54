<?php

namespace App\Entity;

use App\Entity\Traits\NestedSetsTrait;
use App\Entity\Traits\SortableTrait;
use \Doctrine\Common\Collections\Collection;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Section
 */
class Section
{
    use SortableTrait;
    use NestedSetsTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $announce;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $meta_title;

    /**
     * @var string
     */
    private $meta_desc;

    /**
     * @var string
     */
    private $meta_key;

    /**
     * @var boolean
     */
    private $show_tit = true;

    /**
     * @var boolean
     */
    private $show_sub = true;

    /**
     * @var Menu
     */
    private $menu;

    /**
     * @var Collection<Building>
     */
    private $buildings;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var integer
     */
    private $type = 0;

    /**
     * @var string
     */
    private $map_coords;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->buildings = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Section
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Section
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * Set announce
     *
     * @param string $announce
     *
     * @return Section
     */
    public function setAnnounce(?string $announce): self
    {
        $this->announce = $announce;

        return $this;
    }

    /**
     * Get announce
     *
     * @return string
     */
    public function getAnnounce(): ?string
    {
        return $this->announce;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Section
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     *
     * @return Section
     */
    public function setMetaTitle(?string $metaTitle): self
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string
     */
    public function getMetaTitle(): ?string
    {
        return $this->meta_title;
    }

    /**
     * Set metaDesc
     *
     * @param string $metaDesc
     *
     * @return Section
     */
    public function setMetaDesc(?string $metaDesc): self
    {
        $this->meta_desc = $metaDesc;

        return $this;
    }

    /**
     * Get metaDesc
     *
     * @return string
     */
    public function getMetaDesc(): ?string
    {
        return $this->meta_desc;
    }

    /**
     * Set metaKey
     *
     * @param string $metaKey
     *
     * @return Section
     */
    public function setMetaKey(?string $metaKey): self
    {
        $this->meta_key = $metaKey;

        return $this;
    }

    /**
     * Get metaKey
     *
     * @return string
     */
    public function getMetaKey(): ?string
    {
        return $this->meta_key;
    }

    /**
     * Set showTit
     *
     * @param boolean $showTit
     *
     * @return Section
     */
    public function setShowTit(?string $showTit): self
    {
        $this->show_tit = $showTit;

        return $this;
    }

    /**
     * Get showTit
     *
     * @return boolean
     */
    public function getShowTit(): bool
    {
        return $this->show_tit;
    }

    /**
     * Set showSub
     *
     * @param boolean $showSub
     *
     * @return Section
     */
    public function setShowSub(?string $showSub): self
    {
        $this->show_sub = $showSub;

        return $this;
    }

    /**
     * Get showSub
     *
     * @return boolean
     */
    public function getShowSub(): bool
    {
        return $this->show_sub;
    }

    /**
     * Set menu
     *
     * @param Menu $menu
     *
     * @return Section
     */
    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return Menu
     */
    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    /**
     * Add building
     *
     * @param Building $building
     *
     * @return Section
     */
    public function addBuilding(Building $building): self
    {
        $this->buildings[] = $building;

        return $this;
    }

    /**
     * @param Building $building
     * @return Section
     */
    public function removeBuilding(Building $building): self
    {
        $this->buildings->removeElement($building);

        return $this;
    }

    /**
     * Get buildings
     *
     * @return Collection<Building>
     */
    public function getBuildings(): Collection
    {
        return $this->buildings;
    }

    /**
     * @return Section[]
     */
    public function getAllParents(): array
    {
        $parents = array();

        $parent = $this;

        while ($parent) {

            if ($parent = $parent->getParent()) {
                $parents[] = $parent;
            }
        }

        if ($parents) {
            array_pop($parents);
        }

        return $parents;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Section
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * Set mapCoords
     *
     * @param string $mapCoords
     *
     * @return Section
     */
    public function setMapCoords(?string $mapCoords)
    {
        $this->map_coords = $mapCoords;

        return $this;
    }

    /**
     * Get mapCoords
     *
     * @return string
     */
    public function getMapCoords(): ?string
    {
        return $this->map_coords;
    }

    /**
     * @return string
     */
    public function getNameWithLevel(): string
    {
        return str_repeat('--', $this->getLvl()).$this->getName();
    }

    /**
     * @return Collection
     */
    public function getPhotos(): Collection
    {
        return new ArrayCollection();
    }

    /**
     * @throws \Exception
     */
    public function lifecycleRootProtect(): void
    {
        //protect from self-parents
        if ($this->getParent() && $this->getParent()->getId() == $this->getId()) {

            throw new \Exception('Parent can not be parent to himself');
        }

        //protect for delroot
        if (mb_strtolower($this->getUrl()) == 'root' && !$this->getParent()) {

            throw new \Exception('Root can not be change or deleted');
        }
    }
}
