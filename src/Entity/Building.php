<?php

namespace App\Entity;

use App\Entity\Traits\ImageableTrait;
use App\Entity\Traits\SortableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Building
 */
class Building
{
    use ImageableTrait;
    use SortableTrait;

    private $id;

    private $name;

    private $address;

    private $announce;

    private $complTime;

    private $imgCoords;

    private $material;

    private $floors;

    private $description;

    private $district;

    private $plans;

    public function __construct()
    {
        $this->plans = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAnnounce(?string $announce): self
    {
        $this->announce = $announce;

        return $this;
    }

    public function getAnnounce(): ?string
    {
        return $this->announce;
    }

    public function setComplTime(?string $complTime): self
    {
        $this->complTime = $complTime;

        return $this;
    }

    public function getComplTime(): ?string
    {
        return $this->complTime;
    }

    public function setImgCoords(?string $imgCoords): self
    {
        //@TODO check coords format

        $this->imgCoords = $imgCoords;

        return $this;
    }

    public function getImgCoords(): ?string
    {
        return $this->imgCoords;
    }

    public function setMaterial(?string $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function getMaterial(): ?string
    {
        return $this->material;
    }

    public function setFloors(?int $floors): self
    {
        $this->floors = $floors;

        return $this;
    }

    public function getFloors(): ?int
    {
        return $this->floors;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function addPlan(Plan $plan): self
    {
        $this->plans[] = $plan;

        return $this;
    }

    public function removePlan(Plan $plan): self
    {
        $this->plans->removeElement($plan);

        return $this;
    }

    public function getPlans(): Collection
    {
        return $this->plans;
    }

    public function setPlans(Collection $plans): self
    {
        $this->plans = $plans;

        return $this;
    }

    public function setDistrict(District $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getDistrict(): ?District
    {
        return $this->district;
    }

    public function __toString()
    {
        return $this->getName();
    }
}