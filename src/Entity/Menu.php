<?php

namespace App\Entity;

use App\Entity\Traits\SortableTrait;

/**
 * Menu
 */
class Menu
{
    use SortableTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var \App\Entity\Section
     */
    private $section;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Menu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Menu
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set section
     *
     * @param \App\Entity\Section $section
     *
     * @return Menu
     */
    public function setSection(\App\Entity\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \App\Entity\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    public function getFullName()
    {
        if ($this->getName()) {
            return $this->getName();
        }

        return $this->getSection()->getName();
    }
}
