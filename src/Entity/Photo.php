<?php

namespace App\Entity;

use App\Entity\Traits\ImageableTrait;
use App\Entity\Traits\SortableTrait;

class Photo
{
    use SortableTrait;
    use ImageableTrait;

    private $id;

    private $name;

    private $section;

    private $district;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setDistrict(District $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getDistrict(): ?District
    {
        return $this->district;
    }
}
