<?php

namespace App\Entity\Traits;

trait TimestampableTrait
{
    private $created;

    private $updated;

    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    public function setCreatedValue(): self
    {
        $this->created = new \DateTime();
        $this->setUpdatedValue();

        return $this;
    }

    public function setUpdatedValue(): self
    {
        $this->updated = new \DateTime();

        return $this;
    }
}