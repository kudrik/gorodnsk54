<?php

namespace App\Entity\Traits;

trait ImageableTrait
{
    /**
     * Unmapped property to handle image uploads
     */
    private $image;

    private $updated;

    private function setUpdatedValue(): self
    {
        $this->updated = new \DateTime();

        return $this;
    }

    private function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    public function setImage(string $image): self
    {
        if ($image) {
            $this->setUpdatedValue();
        }

        $this->image = $image;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getImagePath(): string
    {
        return '/images/'.strtolower(basename(str_replace('\\','/',get_class($this)))).'/';
    }

    public function getImageSizes(): array
    {
        return [
            'Orig' => [null, null, null],
            'L' => [800, 600, 90],
            'M' => [400, 300, 90],
            'S' => [200, 100, 90]
        ];
    }

    public function getImageExt(): string
    {
        if (isset($this->imageExt)) {
            return $this->imageExt;
        } else {
            return 'jpg';
        }
    }

    public function getImageOrigName(): string
    {
        return $this->getImagePath().$this->getId().'.'.$this->getImageExt();
    }

    public function getImageLName(): string
    {
        return $this->getImagePath().$this->getId().'l.'.$this->getImageExt();
    }

    public function getImageMName(): string
    {
        return $this->getImagePath().$this->getId().'m.'.$this->getImageExt();
    }

    public function getImageSName(): string
    {
        return $this->getImagePath().$this->getId().'s.'.$this->getImageExt();
    }

    public function getImgPreviewTag(): string
    {
        if ($this->getId()>0) {
            return '<a href="'.$this->getImageOrigName().'"><img src="' . $this->getImageSName() . $this->getImageNameCachePostfix() . '" alt="" style="max-width:100px;" class="admin-preview"></a>';
        }

        return '';
    }

    public function getImageNameCachePostfix(): string
    {
        if ($this->getUpdated()) {
            return '?c='.$this->getUpdated()->format('Y-m-d H:i:s');
        }

        return '';
    }
}