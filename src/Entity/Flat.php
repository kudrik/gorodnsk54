<?php

namespace App\Entity;

/**
 * Flat
 */
class Flat
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $floors;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var Plan
     */
    private $plan;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set floors
     *
     * @param string $floors
     *
     * @return Flat
     */
    public function setFloors(?string $floors): self
    {
        $this->floors = $floors;

        return $this;
    }

    /**
     * Get floors
     *
     * @return string
     */
    public function getFloors(): ?string
    {
        return $this->floors;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Flat
     */
    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param Plan|null $plan
     * @return $this
     */
    public function setPlan(Plan $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * @return Plan
     */
    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    public function getDescription(): string
    {
        return 'Этажи: ' . $this->getFloors() . ', цена: ' . number_format($this->getPrice(), 0,'',' ') .' руб.';
    }
}
