<?php

namespace App\Entity;

use App\Entity\Traits\SortableTrait;
use Doctrine\Common\Collections\Criteria;
use App\Entity\Traits\ImageableTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class District
{
    use ImageableTrait;
    use SortableTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $url;

    /**
     * @var boolean
     */
    private $is_public = true;

    /**
     * @var boolean
     */
    private $show_related = true;

    /**
     * @var string
     */
    private $announce;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $map_coords;

    /**
     * @var Collection
     */
    private $buildings;

    /**
     * @var Collection
     */
    private $tags;

    /**
     * @var Collection
     */
    private $users;

    /**
     * @var Collection
     */
    private $photos;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $styles;

    /**
     * @var NULL|int
     */
    private $minPrice;

    /**
     * @var \App\Entity\Company
     */
    private $company;

    /**
     * @var Collection
     */
    private $plans;

    /**
     * @var NULL|string
     */
    private $ipoteka;

    /**
     * @var NULL|string
     */
    private $metrika;

    public function __construct()
    {
        $this->buildings = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->photos = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->plans = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return District
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @param null|string $host
     * @return $this
     */
    public function setHost(?string $host): self
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     * @return $this
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAnnounce(): ?string
    {
        return $this->announce;
    }

    /**
     * @param string|null $announce
     * @return District
     */
    public function setAnnounce(?string $announce): self
    {
        $this->announce = $announce;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return District
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMapCoords(): ?string
    {
        return $this->map_coords;
    }

    /**
     * @param string|null $mapCoords
     * @return District
     */
    public function setMapCoords(?string $mapCoords): self
    {
        $this->map_coords = $mapCoords;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaTitle(): ?string
    {
        return $this->getName();
    }

    /**
     * @return string|null
     */
    public function getMetaDesc(): ?string
    {
        return $this->getAnnounce();
    }

    /**
     * @return string|null
     */
    public function getMetaKey(): ?string
    {
        return '';
    }

    /**
     * @param Building $building
     * @return District
     */
    public function addBuilding(Building $building): self
    {
        $this->buildings[] = $building;

        return $this;
    }

    /**
     * @param Building $building
     * @return District
     */
    public function removeBuilding(Building $building): self
    {
        $this->buildings->removeElement($building);

        return $this;
    }

    /**
     * Get buildings
     *
     * @return Collection
     */
    public function getBuildings(): Collection
    {
        return $this->buildings;
    }

    /**
     * @param Company $company
     * @return District
     */
    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Photo $photo
     * @return District
     */
    public function addPhoto(Photo $photo): self
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * @param Photo $photo
     * @return District
     */
    public function removePhoto(Photo $photo): self
    {
        $this->photos->removeElement($photo);

        return $this;
    }

    /**
     * Get photos
     *
     * @return Collection
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    /**
     * @return bool
     */
    public function getIsPublic(): bool
    {
        return $this->is_public;
    }

    /**
     * @param bool $isPublic
     * @return District
     */
    public function setIsPublic(bool $isPublic): self
    {
        $this->is_public = $isPublic;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return District
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = preg_replace('/[^0-9]/', '', $phone);

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): self
    {
        $this->mobile = preg_replace('/[^0-9]/', '', $mobile);

        return $this;
    }

    /**
     * @return null|string
     */
    public function getStyles(): ?string
    {
        return $this->styles;
    }

    /**
     * @param null|string $styles
     * @return $this
     */
    public function setStyles(?string $styles): self
    {
        $this->styles = $styles;

        return $this;
    }

    /**
     * @return bool
     */
    public function showRelated(): bool
    {
        return $this->show_related;
    }

    /**
     * @param bool $show_related
     * @return District
     */
    public function setShowRelated(bool $show_related): self
    {
        $this->show_related = $show_related;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     * @return District
     */
    public function addTag(Tag $tag): self
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * @param Tag $tag
     * @return District
     */
    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @return int|null
     */
    public function getMinPrice(): ?int
    {
        return $this->minPrice;
    }

    /**
     * @param int|null $minPrice
     * @return District
     */
    public function setMinPrice(?int $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    /*
     * @TODO implement lazy loading
     */
    public function getPlans(): Collection
    {
        $plans = new ArrayCollection();

        foreach ($this->getBuildings() as $building) {
            foreach ($building->getPlans() as $plan) {
                $plans->add($plan);
            }
        }

        return $plans;
    }

    public function setIpoteka(?string $ipoteka): self
    {
        $this->ipoteka = $ipoteka;

        return $this;
    }

    public function getIpoteka(): ?string
    {
        return $this->ipoteka;
    }

    public function getMetrika(): ?string
    {
        return $this->metrika;
    }

    public function setMetrika(?string $metrika): self
    {
        $this->metrika = $metrika;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}