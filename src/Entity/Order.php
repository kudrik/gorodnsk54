<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;

/**
 * Order
 */
class Order
{
    use TimestampableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $contact_name;

    /**
     * @var string
     */
    private $contact_phone;

    /**
     * @var string
     */
    private $theme;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $userinfo;

    /**
     * @var string
     */
    private $referrer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contactName
     *
     * @param string $contactName
     *
     * @return Order
     */
    public function setContactName($contactName)
    {
        $this->contact_name = $contactName;

        return $this;
    }

    /**
     * Get contactName
     *
     * @return string
     */
    public function getContactName()
    {
        return $this->contact_name;
    }

    /**
     * Set contactPhone
     *
     * @param string $contactPhone
     *
     * @return Order
     */
    public function setContactPhone($contactPhone)
    {
        $this->contact_phone = $contactPhone;

        return $this;
    }

    /**
     * Get contactPhone
     *
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contact_phone;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return Order
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Order
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set userinfo
     *
     * @param string $userinfo
     *
     * @return Order
     */
    public function setUserinfo($userinfo)
    {
        $this->userinfo = $userinfo;

        return $this;
    }

    /**
     * Get userinfo
     *
     * @return string
     */
    public function getUserinfo()
    {
        return $this->userinfo;
    }

    /**
     * @return null|string
     */
    public function getReferrer(): ?string
    {
        return $this->referrer;
    }

    /**
     * @param null|string $referrer
     * @return $this
     */
    public function setReferrer(?string $referrer)
    {
        $this->referrer = $referrer;

        return $this;
    }


    /* pseudo fields */

    /**
     * @var Plan
     */
    private $plan;

    /**
     * @param Flat|null $flat
     * @return $this
     */
    public function setPlan(Plan $plan = null)
    {
        $this->plan = $plan;
        if ($plan) {
            $this->setDistrict($plan->getBuilding()->getDistrict());
        }
        return $this;
    }

    /**
     * @return Plan
     */
    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    /**
     * @var
     */
    private $floor;

    /**
     * @param $var
     * @return $this
     */
    public function setFloor($var)
    {
        $this->floor = $var;

        return $this;
    }

    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @var District
     */
    private $district;

    /**
     * @param District|null $district
     * @return $this
     */
    public function setDistrict(?District $district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return District
     */
    public function getDistrict(): ?District
    {
        return $this->district;
    }
}