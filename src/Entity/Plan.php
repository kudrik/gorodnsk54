<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Traits\ImageableTrait;

class Plan
{
    use ImageableTrait;

    private $id;

    private $rooms = 1;

    private $s_total;

    private $s_living;

    private $s_kitchen;

    private $s_loggia;

    private $loggia = false;

    private $description;

    private $flats;

    private $building;

    private $minPrice = 0;

    public function __construct()
    {
        $this->flats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setRooms(int $rooms): self
    {
        $this->rooms = $rooms;

        return $this;
    }

    public function getRooms(): int
    {
        return $this->rooms;
    }

    public function setSTotal(?float $sTotal): self
    {
        $this->s_total = $sTotal;

        return $this;
    }

    public function getSTotal(): ?float
    {
        return $this->s_total;
    }

    public function setSLiving(?float $sLiving): self
    {
        $this->s_living = $sLiving;

        return $this;
    }

    public function getSLiving(): ?float
    {
        return $this->s_living;
    }

    public function setSKitchen(?float $sKitchen): self
    {
        $this->s_kitchen = $sKitchen;

        return $this;
    }

    public function getSKitchen(): ?float
    {
        return $this->s_kitchen;
    }

    public function setSLoggia(?float $sLoggia): self
    {
        $this->s_loggia = $sLoggia;

        $this->setLoggia(boolval($sLoggia));

        return $this;
    }

    public function getSLoggia(): ?float
    {
        return $this->s_loggia;
    }

    public function setLoggia(bool $loggia): self
    {
        $this->loggia = $loggia;

        return $this;
    }

    public function getLoggia(): bool
    {
        return $this->loggia;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function addFlat(Flat $flat): self
    {
        $this->flats[] = $flat;

        return $this;
    }

    public function removeFlat(Flat $flat): self
    {
        $this->flats->removeElement($flat);

        return $this;
    }

    public function getFlats(): Collection
    {
        return $this->flats;
    }

    public function setBuilding(Building $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getBuilding(): ?Building
    {
        return $this->building;
    }

    public function getName(): string
    {
        if ($this->getRooms() == 0) {
            return 'Студия';
        }
        if ($this->getRooms() == 99) {
            return 'Коммерческое помещение';
        }
        return $this->getRooms() . '-к квартира';
    }

    public function getFullName(): string
    {
        $s = $this->getBuilding()->getDistrict()->getName()
            . ', ' . $this->getBuilding()->getAddress()
            . ', ' . $this->getName()
            . ' ' . $this->getSTotal() . ' кв.м';
        if ($this->getMinPrice()) {
            $s .= ', цена: ' . $this->getMinPrice() . ' руб.';
        }

        return $s;
    }

    public function setMinPrice(int $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    public function getMinPrice(): int
    {
        return $this->minPrice;
    }

    public function getSquares(): string
    {
        return $this->getSTotal() . '/' . $this->getSLiving() . '/' . $this->getSKitchen();
    }

    public function __toString()
    {
        return $this->getName();
    }
}
