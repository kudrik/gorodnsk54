<?php

namespace App\Entity;

use App\Entity\Traits\ImageableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Company
{
    use ImageableTrait;

    private $id;

    private $name;

    private $url;

    private $description;

    private $districts;

    public function __construct()
    {
        $this->districts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function addDistrict(District $district): self
    {
        $this->districts[] = $district;

        return $this;
    }

    public function removeDistrict(District $district): self
    {
        $this->districts->removeElement($district);

        return $this;
    }

    public function getDistricts(): Collection
    {
        return $this->districts;
    }
}
