<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
/**
 * User
 */
class User implements UserInterface, \Serializable
{
    use TimestampableTrait;

    const ROLES = ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER'];

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $email = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var array
     */
    private $roles = [];

    /**
     * @var string
     */
    private $restore;

    /**
     * @var \DateTime
     */
    private $restore_to;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @var Collection
     */
    private $districts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->districts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Set restore
     *
     * @param string $restore
     *
     * @return User
     */
    public function setRestore(string $restore): self
    {
        $this->restore = $restore;

        return $this;
    }

    /**
     * Get restore
     *
     * @return string
     */
    public function getRestore(): ?string
    {
        return $this->restore;
    }

    /**
     * @param \DateTime $restoreTo
     * @return User
     */
    public function setRestoreTo(\DateTime $restoreTo): self
    {
        $this->restore_to = $restoreTo;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRestoreTo(): ?\DateTime
    {
        return $this->restore_to;
    }

    /**
     * Get districts
     *
     * @return Collection
     */
    public function getDistricts(): Collection
    {
        return $this->districts;
    }

    /**
     * @param District $district
     * @return User
     */
    public function addDistrict(District $district): self
    {
        $this->districts[] = $district;

        return $this;
    }

    /**
     * @param District $district
     * @return User
     */
    public function removeDistrict(District $district): self
    {
        $this->districts->removeElement($district);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPlainPassword(string $password): self
    {
        $this->plainPassword = $password;

        $this->setUpdatedValue();

        return $this;
    }

    public function getRolesString(): string
    {
        return implode(', ', $this->getRoles());
    }
}
