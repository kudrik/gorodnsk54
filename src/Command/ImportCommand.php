<?php
namespace App\Command;

use App\Entity\Building;
use App\Entity\Flat;
use App\Entity\Plan;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\PageController;
use Symfony\Component\DomCrawler\Crawler;

class ImportCommand extends Command
{
    protected static $defaultName = 'app:import-data';

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

	protected function execute(InputInterface $input, OutputInterface $output)
	{

        $manager = $this->entityManager;


        $content = file_get_contents('data.html');
        $content = str_replace(["\n","\r"],'', $content);

        $building = $manager->getRepository(Building::class)->find(40);

        if (preg_match_all('/data-id="(.*?)"/i', $content, $matches)) {

            foreach ($matches[1] as $row) {

                $item = file_get_contents('https://crm.gk-strizhi.ru/ajax/matrix/widget/element/?container=%23studiobit-matrix-widget-element&container_for_title=title&container_for_h1=h1&guid='.$row);

                $item = json_decode($item, 1)['data']['ELEMENT'];

                $plan = $manager->getRepository(Plan::class)->findOneBy(
                    [
                        'building' => $building,
                        'rooms' => $item['ROOMS'],
                        's_total' => $item['AREA'],
                        's_living' => $item['LIVE_AREA'],
                        's_kitchen' => $item['KITCHEN_AREA'],
                        's_loggia' => $item['LOGGIA_AREA']
                    ]);

                if (!$plan) {
                    $plan = new Plan();
                    $plan->setBuilding($building);
                    $plan->setRooms($item['ROOMS']);
                    $plan->setSTotal($item['AREA']);
                    $plan->setSLiving($item['LIVE_AREA']);
                    $plan->setSKitchen($item['KITCHEN_AREA']);
                    $plan->setSLoggia($item['LOGGIA_AREA']);
                    $plan->setImage('https://crm.gk-strizhi.ru'.@$item['IMAGES'][0]['src']);
                    $manager->persist($plan);
                    $manager->flush();
                }

                $flat = $manager->getRepository(Flat::class)->findOneBy(
                    [
                        'plan' => $plan,
                        'price' => $item['CURRENT_SUM']
                    ]
                );

                if ($flat) {
                    $floors = explode(',', $flat->getFloors());
                    $floors[] = $item['LEVEL'];
                    $flat->setFloors( implode(',', array_unique($floors)));
                } else {
                    $flat = new Flat();
                    $flat->setPlan($plan);
                    $flat->setFloors($item['LEVEL']);
                    $flat->setPrice($item['CURRENT_SUM']);
                    $manager->persist($flat);
                }

                $manager->flush();

                echo $plan->getId();

                echo PHP_EOL;



            }
        }


            return ;

        $manager = $this->getContainer()->get('doctrine')->getManager();

        $content = file_get_contents($this->getContainer()->get('kernel')->getRootDir().'/../src/App/Command/input.txt');

        $content = str_replace(["\n","\r"],'', $content);

        $building = $this->getContainer()->get('doctrine')->getRepository(\App\Entity\Building::class)->find(37);


        if (preg_match_all('/<div class="item-wrap mix".*?data-price="([0-9]*).*?data-square="(.*?)".*?<img.*?realsrc="(.*?)".*?<div class="t-title".*?<a.*?>(.*?)<\/a>.*?<div class="t-title light">(.*?)</i', $content, $matches)) {

            foreach ($matches[1] as $i => $price) {

                $floors_arr = explode('-',trim($matches[5][$i]));

                $floors = intval($floors_arr[0]) . (isset($floors_arr[1]) ? '-'.intval($floors_arr[1]) : '');

                $plan = new \App\Entity\Plan();
                $plan->setBuilding($building);
                $plan->setRooms(
                    substr(trim($matches[4][$i]), 0, 1)
                );
                $plan->setLoggia(true);
                $plan->setSTotal($matches[2][$i]);
                $plan->setImage('https://novo-nsk.ru' . str_replace('preview_','', $matches[3][$i]));

                $manager->persist($plan);


                $flat = new \App\Entity\Flat();
                $flat->setPlan($plan);
                $flat->setPrice($price*1000);
                $flat->setFloors($floors);

                $manager->persist($flat);



                echo $plan->getRooms() .' - '. $plan->getSTotal() . ' ' .$flat->getPrice();
                echo ' '. $floors;
                echo PHP_EOL;
            }

            $manager->flush();

            echo $i;


            /*

            foreach ($matches[2] as $i => $prices) {

                $plan = $this->getContainer()->get('doctrine')->getRepository(\App\Entity\Plan::class)->findOneBy(
                    [
                        'building' => $buildings,
                        's_total' => $matches[1][$i]
                    ]
                );

                echo ' ----- '.$matches[1][$i] .' plan id '.$plan->getId();
                echo PHP_EOL;


                if (preg_match_all('/<tr>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<\/tr>/', $prices, $matches2)) {

                    foreach ($matches2[1] as $j => $floors) {

                        $fl_mass = explode('-', $floors);

                        if (count($fl_mass) == 2) {
                            $floors_str = implode(range($fl_mass[0], $fl_mass[1]), ',');
                        } else {
                            $floors_str = $floors;
                        }


                        if ($plan) {
                            $flat = new \App\Entity\Flat();
                            $flat->setPlan($plan);
                            $flat->setFloors($floors_str);
                            $flat->setPrice(str_replace(' ', '', $matches2[2][$j]));

                            $manager->persist($flat);
                            $manager->flush();

                        }

                    }


                    echo PHP_EOL;
                    echo PHP_EOL;
                }

            }
            */


        }


        return ;

        $building = $this->getContainer()->get('doctrine')->getRepository(\App\Entity\Building::class)->find(36);

	    $file = $this->getContainer()->get('kernel')->getRootDir().'/../src/App/Command/input.txt';

        $content = file_get_contents($file);

        $content = str_replace(["\n","\r"],'', $content);

        if (preg_match_all('/<div class="image.*?<img src="(.*?)".*?<div.*?<span.*?<span.*?<span.*?>(.*?)<\/span>/', $content, $matches)) {

            foreach ($matches[1] as $k=>$img) {

                $plan = new \App\Entity\Plan();
                $plan->setBuilding($building);
                $plan->setImage('https://history.segment-nsk.ru'.$img);
                $plan->setLoggia(true);


                if (preg_match('/^([0-9]{1})/', $matches[2][$k], $match )) {
                    $plan->setRooms($match[1]);
                }

                if (preg_match('/ ([0-9]+\.?[0-9]*)/', $matches[2][$k], $match)) {
                    $plan->setSTotal($match[1]);
                }

                $manager->persist($plan);
                $manager->flush();

            }

            print_R($matches);
        }





        return ;



	    $file = '/home/constantinp/Dropbox/import.csv';



        $i = 0;

        $truncte = true;


        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ';')) !== FALSE) {

                $rowOk = false;

                $ploshads = explode('/', str_replace(',','.',$data[3]));


                if ($building = $this->getContainer()->get('doctrine')->getRepository(\App\Entity\Building::class)->find($data[0])) {

                    if ($plan = $this->getContainer()->get('doctrine')->getRepository(\App\Entity\Plan::class)->findOneBy(
                        array(
                            'building' => $building,
                            //'rooms' => $data[1],
                            's_total' => $ploshads[0],
                            's_living' => $ploshads[1],
                            's_kitchen' => $ploshads[2],
                        )
                    )) {

                        if ($truncte) {

                            foreach ($this->getContainer()->get('doctrine')->getRepository(\App\Entity\Flat::class)->findAll() as $flat) {

                                $manager->remove($flat);
                            }

                            $manager->flush();

                            $truncte = false;
                        }

                        foreach (explode(',',$data[2]) as $floor) {

                            $flat = new \App\Entity\Flat();
                            $flat->setPlan($plan);
                            $flat->setPrice($data[4]);
                            $flat->setFloor($floor);

                            $manager->persist($flat);
                            $manager->flush();
                        }

                        $rowOk = true;
                    }
                }

                if (!$rowOk) {
                    print_R($data);
                }





                $i++;
            }

        }
	}
}