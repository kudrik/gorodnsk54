<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class DistrictRepository extends EntityRepository
{
    public function findAllWithMinPrice(): array
    {
        $districts = [];
        $result = $this->createQueryBuilder('s')
            ->select('s AS district, MIN(f.price) AS minPrice')
            ->leftJoin('s.buildings', 'b')
            ->leftJoin('b.plans', 'p')
            ->leftJoin('p.flats', 'f')
            ->where('s.is_public=1')
            ->groupBy('s.id')
            ->orderBy('s.position','ASC')
            ->getQuery()
            ->getResult();
        foreach ($result as $k => $row) {
            $row['district']->setMinPrice($row['minPrice']);
            $districts[] = $row['district'];
            unset($result[$k]);
        }

        return $districts;
    }
}