<?php

namespace App\Repository;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use App\Entity\District;
use App\Entity\Plan;

class BuildingRepository extends EntityRepository
{
    public function findByDistrictWithFlats(District $district): Collection
    {
        $buildings = $district->getBuildings();

        foreach ($buildings as $building) {
            $building->setPlans(
                new ArrayCollection(
                    $this->getEntityManager()->getRepository(Plan::class)->findByBuildingWithMinPrice($building)
                )
            );
        }
        return $buildings;
    }
}