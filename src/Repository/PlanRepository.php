<?php

namespace App\Repository;

use App\Entity\Plan;
use Doctrine\ORM\EntityRepository;
use App\Entity\Building;
use App\Entity\District;

class PlanRepository extends EntityRepository
{
    public function findByDistrictWithMinPrice(District $district): array
    {
        $plans = [];
        $result = $this->createQueryBuilder('p')
            ->addSelect('b')
            ->addSelect('MIN(f.price) AS minPrice')
            ->innerJoin('p.building', 'b', 'WITH', 'b.district=:district')
            ->innerJoin('p.flats', 'f')
            ->groupBy('p.id')
            ->orderBy('minPrice', 'ASC')
            ->setParameter('district', $district)
            ->getQuery()
            ->getResult();

        foreach ($result as $row) {
            $row[0]->setMinPrice($row['minPrice']);
            $plans[] = $row[0];
        };

        return $plans;
    }

    public function findByBuildingWithMinPrice(Building $building): array
    {
        $plans = [];
        $result = $this->createQueryBuilder('p')
            ->addSelect('MIN(f.price) AS minPrice')
            ->innerJoin('p.flats', 'f')
            ->where('p.building=:building')
            ->groupBy('p.id')
            ->orderBy('minPrice', 'ASC')
            ->setParameter('building', $building)
            ->getQuery()
            ->getResult();

        foreach ($result as $row) {
            $row[0]->setMinPrice($row['minPrice']);
            $row[0]->setBuilding($building);
            $plans[] = $row[0];
        };

        return $plans;
    }
}