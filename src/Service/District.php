<?php

namespace App\Service;

use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\District as DistrictEntity;

class District
{
    protected $doctrine;

    protected $rows = null;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getAll()
    {
        if (is_null($this->rows)) {
            $this->rows = $this->doctrine->getRepository(DistrictEntity::class)->findAllWithMinPrice();
        }

        return $this->rows;
    }
}