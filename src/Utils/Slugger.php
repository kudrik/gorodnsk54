<?php
namespace App\Utils;

use Transliterator;

class Slugger
{
    public static function slugify(?string $string): ?string
    {
        $string = strip_tags($string);

        $rule = 'Cyrillic-Latin';
        $transliterator = Transliterator::create($rule);
        $string = $transliterator->transliterate($string);
        $string = preg_replace(
            '/[^a-z0-9]/', '_', mb_strtolower(trim($string))
        );

        return $string ? $string : null;
    }
}